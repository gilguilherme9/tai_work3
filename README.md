University of Aveiro Departamento de Electrónica, Telecomunicações e Informática MEI

This work was developed for the course of Algorithmic Information Theory.
 
The idea of this lab work was to explore an approach for measuring similarity that is founded
on the notion of algorithmic information (also known as Kolmogorov complexity or algorithmic
entropy).
With the developed system it's possible to test several considered compression algorithms for identifying people's faces given
a set of test images according to a set of reference images for each corresponding subject.


				External Jars and Libraries

For running this project it's required to import to the IDE of choice the external Jar "xz-1.6.jar" 
and the "commons-compress-1.15" library. Both are included in the delivery of the project.


			Arguments: <ImagesDirectory> <Algorithm>

<ImagesDirectory> - The directory where the set of images considered is located (orl_faces defined by default).
<Algorithm> - The compression algorithm to be used (gzip used by default).







