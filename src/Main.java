import compressors.*;
import recognizer.FaceRecognizer;
import datasetReaders.ImageDatasetReader;

public class Main {
    public static void main(String[] args){

        String path = verifyPathArgument(args);
        String compressor = verifyCompressorArgument(args);

        ImageDatasetReader reader = new ImageDatasetReader(path);
        reader.loadImageFiles();
        FaceRecognizer fr = new FaceRecognizer();
        fr.setCompressor(getCompressor(compressor));
        fr.recognizeFaces(reader.getReferenceImages(), reader.getTestImages());
    }

    public static Compressor getCompressor(String comp){
        if(comp.equals("gzip"))
            return new GzipCompressor();
        else if(comp.equals("bzip2"))
            return new Bzip2Compressor();
        else if(comp.equals("zip"))
            return new ZipCompressor();
        else if(comp.equals("lz4"))
            return new Lz4Compressor();
        else if(comp.equals("snappy"))
            return new SnappyCompressor();
        else if(comp.equals("deflate"))
            return new DeflateCompressor();
        else if(comp.equals("xz"))
            return new XzCompressor();
        else if(comp.equals("lzma"))
            return new LzmaCompressor();
        else
            return null;
    }

    public static String verifyPathArgument(String[] args){
        if(args.length >= 1)
            return args[0];
        else
            return "orl_faces";
    }

    public static String verifyCompressorArgument(String[] args){
        if(args.length >= 2)
            return args[1];
        else
            return "gzip";
    }
}
