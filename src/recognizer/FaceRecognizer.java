package recognizer;

import compressors.Compressor;

import java.io.File;
import java.util.List;
import java.util.Map;

public class FaceRecognizer {

    private static int ErrorPerPersonCounter = 0;
    private static int FileNumber = 0;
    private static int totalErrors = 0;

    private Compressor compressor;

    public FaceRecognizer(){};

    public void setCompressor(Compressor compressor) {
        this.compressor = compressor;
    }

    public void recognizeFaces(Map<Integer, List<File>> ReferenceImages, Map<Integer, List<File>> TestImages){
        for(Integer person : TestImages.keySet()){
            List<File> personImages = TestImages.get(person);
            for(File image : personImages){
                recognizePerson(image, ReferenceImages, person);
            }
        }
    }

    public void recognizePerson(File imageToIdentify, Map<Integer, List<File>> ReferenceImages, int psn){
        double minNCD = 2;
        int personID = 0;
        for(Integer person : ReferenceImages.keySet()){
            List<File> referenceImages = ReferenceImages.get(person);
            for(File refImage : referenceImages){
               double NCD =  calculateNCD(imageToIdentify, refImage);
               if(NCD < minNCD){
                   minNCD = NCD;
                   personID = person;
                }
            }
        }
        if(FileNumber%7 == 0) {
            System.out.print(psn + " : ");
            totalErrors += ErrorPerPersonCounter;
            ErrorPerPersonCounter = 0;
        }
        FileNumber++;
        System.out.print(personID+" ");
        if(psn != personID)
            ErrorPerPersonCounter++;
        if(FileNumber%7 == 0) {
            System.out.println("-> " + ErrorPerPersonCounter);
            FileNumber=0;
        }
        if(FileNumber%7 ==0 && psn == 40)
            System.out.println("total errors: "+totalErrors);
    }

    public double calculateNCD(File imageToIdentify, File refImage){
        return compressor.NCD(imageToIdentify, refImage);
    }
}
