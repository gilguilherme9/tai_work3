package datasetReaders;

import java.io.File;
import java.util.*;

public class ImageDatasetReader {

    private List<File> dirFiles;
    private List<File> imgFiles;
    private static String filePath;
    private Map<Integer, List<File>> ReferenceImages;
    private Map<Integer, List<File>> TestImages;

    public ImageDatasetReader(String path){
        this.filePath = path;
        this.ReferenceImages = new HashMap<>();
        this.TestImages = new HashMap<>();
    }

    public void loadImageFiles()  {
        File imagesDirectory = new File(filePath);
        checkIfFileIsDirectory(imagesDirectory);
        dirFiles = Arrays.asList(imagesDirectory.listFiles());
        Collections.sort(dirFiles);
        loadImagesInFiles(dirFiles);
    }

    private void checkIfFileIsDirectory(File dir){
        if(!dir.exists() || !dir.isDirectory()){
            throw new IllegalArgumentException("The File that was provided is not a directory");
        }
    }

    public void loadImagesInFiles(List<File> dirfiles){
        for(File f : dirfiles){
            if(f.isDirectory()){
                imgFiles = Arrays.asList(f.listFiles());
                Collections.sort(imgFiles);
                List<File> refImages = imgFiles.subList(0,3);
                List<File> tImages = imgFiles.subList(3,10);
                ReferenceImages.put(Integer.parseInt(f.getName().replace("s","")), refImages);
                TestImages.put(Integer.parseInt(f.getName().replace("s","")), tImages);
            }
        }
    }

    public Map<Integer, List<File>> getReferenceImages() {
        return ReferenceImages;
    }

    public Map<Integer, List<File>> getTestImages() {
        return TestImages;
    }
}
