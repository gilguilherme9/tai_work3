package compressors;

import java.io.*;
import java.nio.file.Files;
import java.util.zip.GZIPOutputStream;

public class GzipCompressor implements Compressor{

    public GzipCompressor(){};

    @Override
    public double NCD(File imageToIdentify, File refImage) {
        try {
            byte[] imageToID = Files.readAllBytes(imageToIdentify.toPath());
            byte[] refImageB = Files.readAllBytes(refImage.toPath());

            byte[] bothImages = new byte[imageToID.length + refImageB.length];
            System.arraycopy(imageToID, 0, bothImages, 0, imageToID.length);
            System.arraycopy(refImageB, 0, bothImages, imageToID.length, refImageB.length);

            return (double) (compress(bothImages).length - min(compress(imageToID), compress(refImageB)).length) / max(compress(imageToID),compress(refImageB)).length;
        }
        catch(IOException e){
            System.out.println("IOException Occured");
        }
        return 0;
    }

    @Override
    public byte[] compress(byte[] imageData){
        try {
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream(imageData.length);
            GZIPOutputStream zipStream = new GZIPOutputStream(byteStream);
            zipStream.write(imageData);
            zipStream.close();
            byteStream.close();
            return byteStream.toByteArray();
        }
        catch(IOException e){
            System.out.println("IOException Occured");
        }
        return null;
    }

    @Override
    public byte[] max(byte[] imageA, byte[] imageB){
        if(imageB.length > imageA.length)
            return imageB;
        else
            return imageA;
    }

    @Override
    public byte[] min(byte[] imageA, byte[] imageB){
        if(imageB.length < imageA.length)
            return imageB;
        else
            return imageA;
    }
}
