package compressors;

import java.io.File;

public interface Compressor {
    double NCD(File imageToIdentify, File refImage);
    byte[] max(byte[] imageA, byte[] imageB);
    byte[] min(byte[] imageA, byte[] imageB);
    byte[] compress(byte[] imageData);
}
