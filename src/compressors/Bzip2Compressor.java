package compressors;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;


public class Bzip2Compressor implements Compressor{

    public Bzip2Compressor(){}

    @Override
    public double NCD(File imageToIdentify, File refImage) {
        try {
            byte[] imageToID = Files.readAllBytes(imageToIdentify.toPath());
            byte[] refImageB = Files.readAllBytes(refImage.toPath());

            byte[] bothImages = new byte[imageToID.length + refImageB.length];
            System.arraycopy(imageToID, 0, bothImages, 0, imageToID.length);
            System.arraycopy(refImageB, 0, bothImages, imageToID.length, refImageB.length);

            return (double) (compress(bothImages).length - min(compress(imageToID), compress(refImageB)).length) / max(compress(imageToID),compress(refImageB)).length;
        }
        catch(IOException e){
            System.out.println("IOException Occured");
        }
        return 0;
    }

    @Override
    public byte[] compress(byte[] imageData){
        try {
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream(imageData.length);
            BZip2CompressorOutputStream bzipStream = new BZip2CompressorOutputStream(byteStream);
            bzipStream.write(imageData);
            bzipStream.close();
            byteStream.close();
            return byteStream.toByteArray();
        }
        catch(IOException e){
            System.out.println("IOException Occured");
        }
        return null;
    }

    @Override
    public byte[] max(byte[] imageA, byte[] imageB){
        if(imageB.length > imageA.length)
            return imageB;
        else
            return imageA;
    }

    @Override
    public byte[] min(byte[] imageA, byte[] imageB){
        if(imageB.length < imageA.length)
            return imageB;
        else
            return imageA;
    }
}
